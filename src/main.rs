use idx_parser::matrix::*;
use idx_parser::*;
use std::convert::TryInto;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let mut buf: Vec<u8> = vec![];
    let mut images_file = File::open("resources/t10k-images-idx3-ubyte").unwrap();
    images_file.read_to_end(&mut buf).unwrap();
    let images_idx_file = IDXFile::from_bytes(buf).unwrap();
    let image_mat = images_idx_file.matrix_data;

    let mut buf: Vec<u8> = vec![];
    let mut labels_file = File::open("resources/t10k-labels-idx1-ubyte").unwrap();
    labels_file.read_to_end(&mut buf).unwrap();
    let labels_idx_file = IDXFile::from_bytes(buf).unwrap();
    let label_mat = labels_idx_file.matrix_data;

    let mut image_data = [[[0u8; 28]; 28]; 10000];
    for x in 0..10000 {
        for y in 0..28 {
            for z in 0..28 {
                image_data[x][y][z] = image_mat[x][y][z].clone().try_into().unwrap();
            }
        }
    }

    let mut label_data = [0u8; 10000];
    for x in 0..10000 {
        let m: Matrix = *label_mat[x].clone();
        label_data[x] = m.try_into().unwrap();
    }
    println!("{:?}", label_data);

    //for x in 0..10000 {
    //    label_data[x] = label_mat[x].try_into().unwrap();
    //}

    //// Create a new ImgBuf with width: imgx and height: imgy
    //let mut imgbuf = image::ImageBuffer::new(28, 28);

    //// Iterate over the coordinates and pixels of the image
    //for (x, y, pixel) in imgbuf.enumerate_pixels_mut() {
    //    *pixel = image::Luma([d[100][x as usize][y as usize]])
    //}

    //imgbuf.save("drawing.bmp").unwrap();

    //println!("{:?}", d);
}
